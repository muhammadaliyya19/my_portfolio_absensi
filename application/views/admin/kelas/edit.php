<section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12 connectedSortable">
            <form method="post" action="<?=base_url('admin/editkelas/'.$detail->kelas_kode)?>">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?=$title?></h3>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Tingkatan Kelas</label>
                  <?php $kelas = explode(" ", $detail->kelas_tingkat);?>
                  <select name="kelas" class="form-control" required="">
                    <option value="">--Pilih Tingkatan--</option>
                    <option <?php if ($kelas[0] == 'VI') { echo "selected"; }?> value="VI">VI</option>
                    <option <?php if ($kelas[0] == 'VII') { echo "selected"; }?> value="VII">VII</option>
                    <option <?php if ($kelas[0] == 'VIII') { echo "selected"; }?> value="VIII">VIII</option>
                    <option <?php if ($kelas[0] == 'IX') { echo "selected"; }?> value="IX">IX</option>
                    <option <?php if ($kelas[0] == 'X') { echo "selected"; }?> value="X">X</option>
                    <option <?php if ($kelas[0] == 'XI') { echo "selected"; }?> value="XI">XI</option>
                    <option <?php if ($kelas[0] == 'XII') { echo "selected"; }?> value="XII">XII</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <select name="nmkls" class="form-control" required="">
                    <option value="">--Pilih Nama--</option>
                    <?php foreach ($data as $d) { ?>
                      <option <?php if ($detail->kelas_nmkelas == $d->nmkls_kode) { echo "selected"; }?> value="<?=$d->nmkls_kode?>"><?=$d->nmkls_nama?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="card-footer">
                <a href="<?=base_url('admin/kelas')?>" class="btn btn-danger">Kembali</a>
                <button type="simpan" class="btn btn-primary">Simpan</button>
              </div>
            </div>
            </form>
          </section>
        </div>
      </div>
    </section>