<?php
$nama_bulan=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',
             'September','Oktober','November','Desember'];
$ambil_bulan_t=0;
function getJumlahHari($bulan, $tahun){
  $bulan-=1;
  // var_dump($bulan);
  $days = [31,28,31,30,31,30,31,31,30,31,30,31];
  // echo $days[$bulan]; die;
  if ($bulan == 1 && ($tahun % 4) == 0) {
    return 29;
  }else{
    return $days[$bulan];
  }  
}
function getJamKeberapa($jamAbsen)
{
  $jam_ke = null;
  $date = strtotime($jamAbsen);
  $jam = date('H:i:s', $date);
  // Jadwal jam-pel sekolah
  $interval_jampel = [
    ['jam_ke'=>1, 'mulai' => '07.40', 'selesai' => '08.10'],
    ['jam_ke'=>2, 'mulai' => '08.10', 'selesai' => '08.40'],
    ['jam_ke'=>3, 'mulai' => '08.40', 'selesai' => '09.10'],
    ['jam_ke'=>4, 'mulai' => '09.10', 'selesai' => '09.40'],
    ['jam_ke'=>5, 'mulai' => '10.00', 'selesai' => '10.30'],
    ['jam_ke'=>6, 'mulai' => '10.30', 'selesai' => '11.00'],
    ['jam_ke'=>7, 'mulai' => '11.00', 'selesai' => '11.30'],
    ['jam_ke'=>8, 'mulai' => '11.30', 'selesai' => '12.00']
  ];
  for ($i=0; $i < count($interval_jampel); $i++) { 
    $waktuAbsen = DateTime::createFromFormat('H:i:s', $jam);
    $jamMulai   = DateTime::createFromFormat('H.i', $interval_jampel[$i]['mulai']);
    $jamSelesai = DateTime::createFromFormat('H.i', $interval_jampel[$i]['selesai']);
    if ($waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai)
    {
      $jam_ke = $interval_jampel[$i]['jam_ke'];
    }
  }
  return $jam_ke;  
}

// echo getJamKeberapa('08:47:16')==1 ? "oke" : 'fail';
 ?>
<!--main-container-part-->
<div id="content">
  <div class="container">
    <div class="span11">
      <div class="widget-box">
        <!-- <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
          <h5>Cari</h5>
        </div> -->
        <div class="widget-content container">
          <!-- <form method="get">
            <div class="control-group">
              <label class="control-label">Pilih Kelas :</label>
              <br>
              <select name="kelas">
                <?php
                // $query_kelas=mysqli_query($konek,"SELECT * FROM kelas");
                // while ($data_kelas=mysqli_fetch_array($query_kelas)) {
                //   echo "<option value='$data_kelas[kelas_kode]'>$data_kelas[kelas_nama]</option>";
                // }
                 ?>
              </select>
            </div>
            <div class="control-group">
              <label class="control-label">Bulan :</label><br>
              <select name="bulan">
                <?php
                // $nama_bulan=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',
                //             'September','Oktober','November','Desember'];
                // for($nomor_bulan=1;$nomor_bulan<=12;$nomor_bulan++){
                //   if($nomor_bulan<10){
                //   echo "<option value='0$nomor_bulan'>".$nama_bulan[$nomor_bulan-1]."</option>";
                // }else {
                //     echo "<option value='$nomor_bulan'>".$nama_bulan[$nomor_bulan-1]."</option>";
                //   }
                // }
                 ?>
              </select>
            </div><br>
            <div class="control-group">
              <label class="control-label">Tahun :</label>
              <div class="controls">
                <input type="number"placeholder="Tahun"name="tahun" value="<?PHP echo date('Y'); ?>"/>
              </div>
            </div><br>
            <div class="form-actions">
              <button type="submit" class="btn btn-success"name="cari">Cari</button>
            </div>
          </form> -->
                <form action="" method="post" target="">
                  <input type="hidden" name="cari" value="tes">
                  <div class="row">
                    <span class="h3">Filter Data </span>                                  
                    <div class="col"></div>                  
                    <div class="col-2">
                      <select name="kelas" id="kelas" class="form-control">
                        <option value="">Nama Kelas</option>
                        <?php foreach ($data_kelas as $kls):?>
                        <option value="<?=$kls->kelas_kode; ?>" <?php if ($_POST != null && $_POST['kelas'] == $kls->kelas_kode) {echo " selected";} ?>><?=$kls->kelas_tingkat . ' - ' . $kls->nmkls_nama; ?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <div class="col-2">
                      <select name="bulan" id="bulan" class="form-control">
                        <option value="">Bulan</option>
                        <option value="1" <?php if ($_POST != null && $_POST['bulan'] == 1) {echo " selected";} ?>>Januari</option>
                        <option value="2" <?php if ($_POST != null && $_POST['bulan'] == 2) {echo " selected";} ?>>Februari</option>
                        <option value="3" <?php if ($_POST != null && $_POST['bulan'] == 3) {echo " selected";} ?>>Maret</option>
                        <option value="4" <?php if ($_POST != null && $_POST['bulan'] == 4) {echo " selected";} ?>>April</option>
                        <option value="5" <?php if ($_POST != null && $_POST['bulan'] == 5) {echo " selected";} ?>>Mei</option>
                        <option value="6" <?php if ($_POST != null && $_POST['bulan'] == 6) {echo " selected";} ?>>Juni</option>
                        <option value="7" <?php if ($_POST != null && $_POST['bulan'] == 7) {echo " selected";} ?>>Juli</option>
                        <option value="8" <?php if ($_POST != null && $_POST['bulan'] == 8) {echo " selected";} ?>>Agustus</option>
                        <option value="9" <?php if ($_POST != null && $_POST['bulan'] == 9) {echo " selected";} ?>>September</option>
                        <option value="10" <?php if ($_POST != null && $_POST['bulan'] == 10) {echo " selected";} ?>>Oktober</option>
                        <option value="11" <?php if ($_POST != null && $_POST['bulan'] == 11) {echo " selected";} ?>>November</option>
                        <option value="12" <?php if ($_POST != null && $_POST['bulan'] == 12) {echo " selected";} ?>>Desember</option>
                      </select>
                    </div>
                    <div class="col-lg-1">                  
                      <select name="tahun" id="tahun" class="form-control">
                        <?php for ($i=(int)date('Y'); $i < 2030; $i++) { ?>
                          <option value="<?=$i; ?>" <?php if ($_POST != null && $_POST['tahun'] == $i) {echo " selected";} ?>><?=$i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-lg-1">
                      <button type="submit" name="cari" value="cari" class="btn btn-success" style="width: 100%;">Cari</button>
                    </div>
                    <div class="col-lg-1">
                      <button type="submit" name="cetak" value="print" class="btn btn-success" style="width: 100%;">Cetak</button>
                    </div>
                  </div>
                </form>                
        </div>
      </div>
    </div>
  </div>

  <!-- Untuk function GET, datatable awal / original -->
<?php if(isset($_GET['cari'])){ ?>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="widget-content">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nama Bulan</th>
                  <th>Print</th>
                  <th>Hapus</th>
                </tr>
              </thead>
              <tbody>
                <tr class="odd gradeX">
                  <?php $bulan=intval($_GET['bulan']); ?>
                  <td><?php echo  "$_GET[tahun]-$nama_bulan[$bulan]"; ?></td>
                  <td>
                    <?php
                    $query_kelas=mysqli_query($konek,"SELECT * FROM kelas WHERE kelas_kode='$_GET[kelas_nama]'");
                    while ($data_kelas=mysqli_fetch_assoc($query_kelas)) { ?>
                    <a href="report.php?bulan=<?php echo "$_GET[bulan]&tahun=$_GET[tahun]&kelas=$_GET[kelas_nama]"; ?>"target="_blank">
                    <span class="glyphicon glyphicon-print"></span><b><?PHP echo $data_kelas['kelas_nama']; ?></b></a><?PHP } ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<!-- Penanganan method POST, view ini yang akan ditampilkan -->
<?php 
  if($_POST!=null){
    function getStatusHadir($nissiswa, $jam_ke, $absens, $tanggal){
      $status_hadir = "-";
      $jam_ke -= 1;
      // $date = strtotime($jamAbsen);
      // $jam = date('H:i:s', $date);
      // Jadwal jam-pel sekolah
      $interval_jampel = [
        ['jam_ke'=>1, 'mulai' => '07.40', 'selesai' => '08.10'],
        ['jam_ke'=>2, 'mulai' => '08.10', 'selesai' => '08.40'],
        ['jam_ke'=>3, 'mulai' => '08.40', 'selesai' => '09.10'],
        ['jam_ke'=>4, 'mulai' => '09.10', 'selesai' => '09.40'],
        ['jam_ke'=>5, 'mulai' => '10.00', 'selesai' => '10.30'],
        ['jam_ke'=>6, 'mulai' => '10.30', 'selesai' => '11.00'],
        ['jam_ke'=>7, 'mulai' => '11.00', 'selesai' => '11.30'],
        ['jam_ke'=>8, 'mulai' => '11.30', 'selesai' => '12.00']
      ];
      // for ($i=0; $i < count($interval_jampel); $i++) { 
      $jamMulai   = DateTime::createFromFormat('H.i', $interval_jampel[$jam_ke]['mulai']);
      $jamSelesai = DateTime::createFromFormat('H.i', $interval_jampel[$jam_ke]['selesai']);
      foreach ($absens as $a) {              
        $date = strtotime($a->absensi_waktu);
        $jam  = date('H:i:s', $date);  
        $tanggal_data  = date('Y:m:d', $date);  
        $waktuAbsen = DateTime::createFromFormat('H:i:s', $jam);        
        // if ($a->siswa_nis == $nissiswa && $waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai && $tanggal_data == $tanggal){
        
        if ($a->siswa_nis == $nissiswa && $tanggal_data == $tanggal && $waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai){
          // echo "Found it<br>";
          // echo 'Param tanggal = ' . $tanggal . " vs Tanggal_data = ". $tanggal_data. "<br><br>";
          $status_hadir = $a->detail_kehadiran;        
          
        }
      }
      return $status_hadir;  
    }

    // echo getStatusHadir(1, 3, $absen, '2022:01:25'); die;
?>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Rekap Absensi</h5>
          </div>
          <div class="widget-content" style="width: 100%; overflow:scroll;">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th rowspan="2">Nama Siswa</th>
                  <th>Tanggal</th>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    for ($i=1; $i <= $jumlah_hari; $i++) {                   
                      echo "<th colspan='8' class='text-center'>".$i."</th>";
                    }
                  ?>
                  <th rowspan="2">Jml Hadir</th>
                  <th rowspan="2">Jml Izin</th>
                  <th rowspan="2">Jml Sakit</th>
                  <th rowspan="2">Jml Alpa</th>
                </tr>
                <tr>
                  <th>Jam ke-</th>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    for ($i=1; $i <= $jumlah_hari; $i++){
                  ?>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                  <?php
                    }
                  ?>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($kelas_siswa as $ks):?>
              <tr>                    
                  <td colspan="2"><?=$ks->siswa_nama?></td>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    $jml_hadir = 0;             
                    $jml_izin = 0;             
                    $jml_sakit = 0;             
                    $jml_alpa = 0;                               
                    for ($i=1; $i <= $jumlah_hari; $i++) {      
                      $hadir = 0; $izin = 0; $sakit = 0; $alpa = 0;
                      for ($j=1; $j <= 8; $j++) {      
                        $tgl = $i < 10 ? '0'.$i : $i;
                        $bulan = (int)$_POST['bulan'] < 10 ? '0'.$_POST['bulan'] : $_POST['bulan'];
                        $tanggal = $_POST['tahun'].':'.$bulan.':'.$tgl;
                        $kehadiran = getStatusHadir($ks->siswa_nis, $j, $absen, $tanggal);
                        if ($kehadiran == "H") { $hadir++;} 
                        if ($kehadiran == "I") { $izin++;}
                        if ($kehadiran == "S") { $sakit++;}
                        if ($kehadiran == "A") { $alpa++;}
                        echo "<td>".$kehadiran."</td>";                        
                      } 
                      if ($hadir > 0) { $jml_hadir++;} 
                      if ($izin > 0) { $jml_izin++;}
                      if ($sakit > 0) { $jml_sakit++;}
                      if ($alpa > 0) { $jml_alpa++;}
                    }
                  ?>
                  <td><?=$jml_hadir;?></td>
                  <td><?=$jml_izin;?></td>
                  <td><?=$jml_sakit;?></td>
                  <td><?=$jml_alpa;?></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }else{ ?>
  <div class="row mt-5">
    <div class="col mt-3" align="center">
      <h5>Silakan Pilih Kelas, Nama Kelas, Bulan dan Tahun lalu tekan Cari untuk melihat data.</h5>
      <h5>Untuk cetak data silakan tekan cetak.</h5>
    </div>
  </div>
<?php }?>
</div>
<!--end-main-container-part-->

