<!DOCTYPE html>
<html lang="en">

<head>  
  <title>Cetak Laporan</title>
  <link href='logo.png' rel='icon' type='image/x-icon'/>
  <style type="text/css">
    body{
      font-family: Arial;
    }
    table{
      border-collapse: collapse;
    }

    @media print{
      .no-print{
        display: none;
      }
    }
  </style>  
</head>

<!-- PHP SCRIPT -->
<?php
$nama_bulan=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',
             'September','Oktober','November','Desember'];
$ambil_bulan_t=0;
function getJumlahHari($bulan, $tahun){
  $bulan-=1;
  // var_dump($bulan);
  $days = [31,28,31,30,31,30,31,31,30,31,30,31];
  // echo $days[$bulan]; die;
  if ($bulan == 1 && ($tahun % 4) == 0) {
    return 29;
  }else{
    return $days[$bulan];
  }  
}
function getJamKeberapa($jamAbsen)
{
  $jam_ke = null;
  $date = strtotime($jamAbsen);
  $jam = date('H:i:s', $date);
  // Jadwal jam-pel sekolah
  $interval_jampel = [
    ['jam_ke'=>1, 'mulai' => '07.40', 'selesai' => '08.10'],
    ['jam_ke'=>2, 'mulai' => '08.10', 'selesai' => '08.40'],
    ['jam_ke'=>3, 'mulai' => '08.40', 'selesai' => '09.10'],
    ['jam_ke'=>4, 'mulai' => '09.10', 'selesai' => '09.40'],
    ['jam_ke'=>5, 'mulai' => '10.00', 'selesai' => '10.30'],
    ['jam_ke'=>6, 'mulai' => '10.30', 'selesai' => '11.00'],
    ['jam_ke'=>7, 'mulai' => '11.00', 'selesai' => '11.30'],
    ['jam_ke'=>8, 'mulai' => '11.30', 'selesai' => '12.00']
  ];
  for ($i=0; $i < count($interval_jampel); $i++) { 
    $waktuAbsen = DateTime::createFromFormat('H:i:s', $jam);
    $jamMulai   = DateTime::createFromFormat('H.i', $interval_jampel[$i]['mulai']);
    $jamSelesai = DateTime::createFromFormat('H.i', $interval_jampel[$i]['selesai']);
    if ($waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai)
    {
      $jam_ke = $interval_jampel[$i]['jam_ke'];
    }
  }
  return $jam_ke;  
}

  if($_POST!=null){
    $namakelas="";
    foreach ($data_kelas as $dk) {
      if ($dk->kelas_kode == $_POST['kelas']) {
        $namakelas.=$dk->kelas_tingkat." - ".$dk->nmkls_nama;
      }
    }

    function getStatusHadir($nissiswa, $jam_ke, $absens, $tanggal){
      $status_hadir = "-";
      $jam_ke -= 1;
      // $date = strtotime($jamAbsen);
      // $jam = date('H:i:s', $date);
      // Jadwal jam-pel sekolah
      $interval_jampel = [
        ['jam_ke'=>1, 'mulai' => '07.40', 'selesai' => '08.10'],
        ['jam_ke'=>2, 'mulai' => '08.10', 'selesai' => '08.40'],
        ['jam_ke'=>3, 'mulai' => '08.40', 'selesai' => '09.10'],
        ['jam_ke'=>4, 'mulai' => '09.10', 'selesai' => '09.40'],
        ['jam_ke'=>5, 'mulai' => '10.00', 'selesai' => '10.30'],
        ['jam_ke'=>6, 'mulai' => '10.30', 'selesai' => '11.00'],
        ['jam_ke'=>7, 'mulai' => '11.00', 'selesai' => '11.30'],
        ['jam_ke'=>8, 'mulai' => '11.30', 'selesai' => '12.00']
      ];
      // for ($i=0; $i < count($interval_jampel); $i++) { 
      $jamMulai   = DateTime::createFromFormat('H.i', $interval_jampel[$jam_ke]['mulai']);
      $jamSelesai = DateTime::createFromFormat('H.i', $interval_jampel[$jam_ke]['selesai']);
      foreach ($absens as $a) {              
        $date = strtotime($a->absensi_waktu);
        $jam  = date('H:i:s', $date);  
        $tanggal_data  = date('Y:m:d', $date);  
        $waktuAbsen = DateTime::createFromFormat('H:i:s', $jam);        
        // if ($a->siswa_nis == $nissiswa && $waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai && $tanggal_data == $tanggal){
        
        if ($a->siswa_nis == $nissiswa && $tanggal_data == $tanggal && $waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai){
          // echo "Found it<br>";
          // echo 'Param tanggal = ' . $tanggal . " vs Tanggal_data = ". $tanggal_data. "<br><br>";
          $status_hadir = $a->detail_kehadiran;        
          
        }
      }
      return $status_hadir;  
    }

    // echo getStatusHadir(1, 3, $absen, '2022:01:25'); die;
?>

<body style="margin: 50px;">
    <table cellpadding="4" cellspacing="0" width="100%">
      <tr>
        <td style="width: 10%"><img src="<?=base_url();?>assets/img/logomts.png" alt="" style="height:100px;"></td>
        <td align="center" style="width: 80%">
          <b>REKAP PRESENSI SISWA</b><br>
          <strong>MTs DARUL HIKAM</strong><br>
          <span>Jl. Sukorame. 1, Srigonco, Bantur, Malang<br>Telp. 085655281615</span>
        </td>  
        <td style="width: 10%">
          <a href="#" class="no-print" onclick="window.print();">Cetak/Print</a><br>
          <a href='<?=base_url()."admin/laporanpresensi";?>' class="no-print">Kembali</a><br>
        </td>
      </tr>
    </table>
    <hr style="height: 2px; background-color: black; border: 0; color: black;">
    <div align="center">
      <h3 class="m-0 font-weight-bold text-primary">Rekap Presensi Kelas <?= $namakelas;?> - <?=$nama_bulan[(int)$_POST['bulan']-1] ." ". $_POST['tahun']; ?> <!-- PP. Hidayatul Mubtadi'in Turen --> </h3>
    </div>


          <div class="" style="width: 100%;">
            <!-- <table class="table table-bordered table-striped" > -->
            <table border="1" cellpadding="2" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th rowspan="2">Nama Siswa</th>
                  <th>Tanggal</th>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    for ($i=1; $i <= $jumlah_hari; $i++) {                   
                      echo "<th colspan='8' class='text-center'>".$i."</th>";
                    }
                  ?>
                  <th rowspan="2">Jml Hadir</th>
                  <th rowspan="2">Jml Izin</th>
                  <th rowspan="2">Jml Sakit</th>
                  <th rowspan="2">Jml Alpa</th>
                </tr>
                <tr>
                  <th>Jam ke-</th>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    for ($i=1; $i <= $jumlah_hari; $i++){
                  ?>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                  <?php
                    }
                  ?>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($kelas_siswa as $ks):?>
              <tr>                    
                  <td colspan="2"><?=$ks->siswa_nama?></td>
                  <?php
                    $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    $jml_hadir = 0;             
                    $jml_izin = 0;             
                    $jml_sakit = 0;             
                    $jml_alpa = 0;                               
                    for ($i=1; $i <= $jumlah_hari; $i++) {      
                      $hadir = 0; $izin = 0; $sakit = 0; $alpa = 0;
                      for ($j=1; $j <= 8; $j++) {      
                        $tgl = $i < 10 ? '0'.$i : $i;
                        $bulan = (int)$_POST['bulan'] < 10 ? '0'.$_POST['bulan'] : $_POST['bulan'];
                        $tanggal = $_POST['tahun'].':'.$bulan.':'.$tgl;
                        $kehadiran = getStatusHadir($ks->siswa_nis, $j, $absen, $tanggal);
                        if ($kehadiran == "H") { $hadir++;} 
                        if ($kehadiran == "I") { $izin++;}
                        if ($kehadiran == "S") { $sakit++;}
                        if ($kehadiran == "A") { $alpa++;}
                        echo "<td>".$kehadiran."</td>";                        
                      } 
                      if ($hadir > 0) { $jml_hadir++;} 
                      if ($izin > 0) { $jml_izin++;}
                      if ($sakit > 0) { $jml_sakit++;}
                      if ($alpa > 0) { $jml_alpa++;}
                    }
                  ?>
                  <td><?=$jml_hadir;?></td>
                  <td><?=$jml_izin;?></td>
                  <td><?=$jml_sakit;?></td>
                  <td><?=$jml_alpa;?></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>

            <!-- Bagian footer, untuk tandatangan -->
            <hr style="height: 2px; background-color: black; border: 0; color: black;">
            <table cellpadding="4" cellspacing="0" width="100%">
              <tr>
                <th style="width: 35%" style="text-align: left;">Kepala MTs DARUL HIKAN<br><br><br><br></th>
                <td style="width: 30%;">&nbsp</td>
                <th style="width: 35%" style="text-align: left;">WALI KELAS<br><br><br><br></th>
              </tr>
              <br>
              <br>
              <br>
              <br>
              <tr>
                <th style="width: 20%;">______________________</th>
                <td style="width: 60%"></td>
                <th style="width: 20%;">______________________</th>
              </tr>    
            </table>
          </div>
     
<?php }?>
</div>
<!--end-main-container-part-->
</body>

</html>
