<section class="content">
    <div class="container-fluid">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><?=$title?></h3>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="myTable" class="table table-bordered table-flush">
                  <thead class="thead-light">
                    <th width="1%">No</th>
                    <th>Nama Siswa</th>
                    <th>Nomor Hp Wali</th>
                    <th>Status</th>
                  </thead>
                  <tbody>
                    <?php $i=1; 
                    foreach ($data as $d) {  ?>
                    <tr>
                      <td><?=$i++?></td>
                      <td><?=$d->nama?></td>
                      <td><?=$d->nomor?></td>
					  <td><?php
					      if ($d->status=="TERKIRIM"){
					          echo '<span class="badge badge-success">Terkirim</span>';
					      }else if($d->status=="MENUNGGU JADWAL"){
					          echo '<span class="badge badge-info">Menunggu</span>';
					      }else if($d->status=="GAGAL"){
					          echo '<span class="badge badge-danger">GAGAL</span>';
					      }
					      ?>
					  </td>
                    </tr>
                    <?php } ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>