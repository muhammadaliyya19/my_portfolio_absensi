<style id="table_style" type="text/css">
    body
    {
        font-family: Arial;
        font-size: 10pt;
    }
    table
    {
        border: 1px solid #ccc;
        border-collapse: collapse;
    }
    table th
    {
        background-color: #F7F7F7;
        color: #333;
        font-weight: bold;
    }
    table th, table td
    {
        padding: 5px;
        border: 1px solid #ccc;
    }
</style>
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"><?=$title?></h3>
            
          </div>
          <div id="dvContents" class="card-body row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Mata Pelajaran: </label>
				<label><?=$data->mapel_nama?></label>
				<br>
				<label>Materi Pelajaran: </label>
                  <?php foreach ($materi as $m) { ?>
                      <label> <?php if ($m->materi_id == $dataabsen->absensi_materi) { echo $m->materi_judul; }?> </label>
                  <?php } ?>
				<br>
				<label>Guru Mata Pelajaran: </label>
                <label><?=ucfirst($this->session->userdata('nama'))?></label>
				<br>
				<label>Waktu Absensi: </label>
				<label><?=$dataabsen->absensi_waktu ." / ". date('F',strtotime($dataabsen->absensi_waktu)) . " / Minggu ke - ". $dataabsen->minggu_ke?></label>
              </div>              
            </div>
            <div class="col-md-12"><br></div>
            <div  class="col-md-12">
              <table class="table table-bordered table-flush text-center">
                <thead class="thead-light">
                  <th>NIS</th>
                  <th>Nama Siswa</th>
                  <th>Hadir</th>
                  <th>Izin</th>
                  <th>Sakit</th>
                  <th>Alpa</th>
                </thead> 
                <tbody>
                  <?php $i=0; foreach ($list as $d) { 
                  $cek = $this->db->get_where('detail_absensi',['detail_absensi'=>$dataabsen->absensi_id,'detail_nis'=>$d->siswa_nis])->row();
                  ?>
                  <tr>
                    <input type="hidden" value="<?=$cek->detail_id?>" name="id<?=$i?>" disabled>
                    <td><?=$d->siswa_nis?></td>
                    <td><?=$d->siswa_nama?></td>
                    <td><?php if ($cek->detail_kehadiran == 'H') { echo "✅"; }else{echo "🅾";}?> </td>
					<td><?php if ($cek->detail_kehadiran == 'I') { echo "✅"; }else{echo "🅾";}?> </td>
					<td><?php if ($cek->detail_kehadiran == 'S') { echo "✅"; }else{echo "🅾";}?> </td>
					<td><?php if ($cek->detail_kehadiran == 'A') { echo "✅"; }else{echo "🅾";}?> </td>
                  </tr>
                  <?php $i++; } ?>
                  
                </tbody>
              </table>            
            </div>
          </div>
          <div class="card-footer">
            <a href="<?=base_url('guru/absensi/'.$data->mapelguru_id)?>" class="btn btn-danger">Kembali</a>
			<input type="button" class="btn btn-info" onclick="PrintTable();" value="Print"/>

          </div>
        </div>
      </section>
    </div>
  </div>
</section>

<script type="text/javascript">
    function PrintTable() {
        var printWindow = window.open('', '', '');
        printWindow.document.write('<html><head><title>Table Contents</title>');
 
        //Print the Table CSS.
        var table_style = document.getElementById("table_style").innerHTML;
        printWindow.document.write('<style type = "text/css">');
        printWindow.document.write(table_style);
        printWindow.document.write('</style>');
        printWindow.document.write('</head>');
 
        //Print the DIV contents i.e. the HTML Table.
        printWindow.document.write('<body>');
        var divContents = document.getElementById("dvContents").innerHTML;
        printWindow.document.write(divContents);
        printWindow.document.write('</body>');
 
        printWindow.document.write('</html>');
        printWindow.document.close();
        printWindow.print();
    }
</script>