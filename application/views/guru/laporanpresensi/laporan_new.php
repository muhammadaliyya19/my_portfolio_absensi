<!-- PHP SCRIPTS -->
<?php
$nama_bulan=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',
             'September','Oktober','November','Desember'];
$ambil_bulan_t=0;
function getJumlahHari($bulan, $tahun){
  $bulan-=1;
  // var_dump($bulan);
  $days = [31,28,31,30,31,30,31,31,30,31,30,31];
  // echo $days[$bulan]; die;
  if ($bulan == 1 && ($tahun % 4) == 0) {
    return 29;
  }else{
    return $days[$bulan];
  }  
}
function getJamKeberapa($jamAbsen)
{
  $jam_ke = null;
  $date = strtotime($jamAbsen);
  $jam = date('H:i:s', $date);
  // Jadwal jam-pel sekolah
  $interval_jampel = [
    ['jam_ke'=>1, 'mulai' => '07.40', 'selesai' => '08.10'],
    ['jam_ke'=>2, 'mulai' => '08.10', 'selesai' => '08.40'],
    ['jam_ke'=>3, 'mulai' => '08.40', 'selesai' => '09.10'],
    ['jam_ke'=>4, 'mulai' => '09.10', 'selesai' => '09.40'],
    ['jam_ke'=>5, 'mulai' => '10.00', 'selesai' => '10.30'],
    ['jam_ke'=>6, 'mulai' => '10.30', 'selesai' => '11.00'],
    ['jam_ke'=>7, 'mulai' => '11.00', 'selesai' => '11.30'],
    ['jam_ke'=>8, 'mulai' => '11.30', 'selesai' => '12.00']
  ];
  for ($i=0; $i < count($interval_jampel); $i++) { 
    $waktuAbsen = DateTime::createFromFormat('H:i:s', $jam);
    $jamMulai   = DateTime::createFromFormat('H.i', $interval_jampel[$i]['mulai']);
    $jamSelesai = DateTime::createFromFormat('H.i', $interval_jampel[$i]['selesai']);
    if ($waktuAbsen > $jamMulai && $waktuAbsen < $jamSelesai)
    {
      $jam_ke = $interval_jampel[$i]['jam_ke'];
    }
  }
  return $jam_ke;  
}

  if($_POST!=null){
    $namakelas="";
    foreach ($data_kelas as $dk) {
      if ($dk->kelas_kode == $_POST['kelas']) {
        $namakelas.=$dk->kelas_tingkat." - ".$dk->nmkls_nama;
      }
    }

    // function getStatusHadir($nissiswa, $absens, $tanggal){
    function getStatusHadir($nissiswa, $absens, $tahunajaran, $bulan, $minggu_ke){
        $status_hadir = "-";      
        foreach ($absens as $a) {              
            $date = strtotime($a->absensi_waktu);
            $bulan_data  = date('m', $date);  
            if ($a->siswa_nis == $nissiswa && $a->tahunajaran_kode == $tahunajaran && $a->minggu_ke == $minggu_ke && $bulan_data == $bulan){
                // echo "Found it<br>";
                // echo 'Param tanggal = ' . $tanggal . " vs Tanggal_data = ". $tanggal_data. "<br><br>";
                $status_hadir = $a->detail_kehadiran;                
            }
        }      
        return $status_hadir;   
    }
    // echo getStatusHadir(1, 3, $absen, '2022:01:25'); die;
?>

<!-- BEGIN -->
<!DOCTYPE html>
<html lang="en">

<head>  
  <title>Cetak Rekap Presensi Kelas <?= $namakelas;?> - <?=$absen[0]->tahunajaran_nama; ?></title>
  <link href='logo.png' rel='icon' type='image/x-icon'/>
  <style type="text/css">
    body{
      font-family: Arial;
    }
    
    table{
      border-collapse: collapse;
    }

    @media print{
      @page { size: landscape; }
      .no-print{
        display: none;
      }
    }
  </style>  
</head>
  <body style="margin: 50px;">
    <table cellpadding="4" cellspacing="0" width="100%">
      <tr>
        <td style="width: 10%"><img src="<?=base_url();?>assets/img/logomts.png" alt="" style="height:100px;"></td>
        <td align="center" style="width: 80%">
          <b>REKAP PRESENSI SISWA</b><br>
          <strong>MTs DARUL HIKAM</strong><br>
          <span>Jl. Sukorame. 1, Srigonco, Bantur, Malang<br>Telp. 085655281615</span>
        </td>  
        <td style="width: 10%">
          <a href="#" class="no-print" onclick="window.print();">Cetak/Print</a><br>
          <a href='<?=base_url()."guru/laporanpresensi";?>' class="no-print">Kembali</a><br>
        </td>
      </tr>
    </table>
    <hr style="height: 2px; background-color: black; border: 0; color: black;">
    <div align="center">
      <h3 class="m-0 font-weight-bold text-primary">Rekap Presensi Kelas <?= $namakelas;?> - <?=$absen[0]->tahunajaran_nama; ?> <!-- PP. Hidayatul Mubtadi'in Turen --> </h3>
    </div>
  
          <div class="" style="width: 100%;">
            <!-- <table class="table table-bordered table-striped" > -->
            <table border="1" cellpadding="4" cellspacing="0" width="100%">
            <!-- <table class="table table-bordered table-striped"> -->
              <thead>
                <tr>
                  <th rowspan="2" width="50">Nama Siswa</th>
                  <th>Bulan</th>
                  <?php
                    // $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    $semester = substr($absen[0]->tahunajaran_nama,0,6);
                    $bulan_list = [];
                    if($semester == "Ganjil"){
                        // echo "Ganjil"; die;
                        $bulan_list = [[7,'Juli'], [8,'Agustus'], [9,'September'], [10,'Oktober'],[11,'November'], [12, 'Deseember']];
                    }else{
                            // echo "Genap"; die;
                            $bulan_list = [[1,'Januari'], [2, 'Februari'], [3, 'Maret'], [4, 'April'],[5,'Mei'], [6,'Juni']];
                    }
                    for ($i=0; $i < count($bulan_list); $i++) {  
                        // $id = getIdAbsen($kelas_siswa[0]->siswa_nis, $absen, $i);
                        $id = 0;
                        // echo "<th class='text-center'>".$bulan_list[$i]."<br><a href='".base_url('guru/detailabsensi/'.$id). "' class='btn btn-warning btn-sm'><span class='fa fa-eye'></span></a></th>";
                        echo "<th colspan='5' class='text-center'>".$bulan_list[$i][1]."</th>";
                    }
                  ?>
                  <th rowspan="2">Jml Hadir</th>
                  <th rowspan="2">Jml Izin</th>
                  <th rowspan="2">Jml Sakit</th>
                  <th rowspan="2">Jml Alpa</th>
                </tr>
                  <th>Minggu ke</th>
                    <?php
                    for ($i=0; $i < count($bulan_list); $i++) {  
                        for ($j=1; $j<=5; $j++) {  
                            echo "<th class='text-center'>".$j."</th>";
                        }
                    }
                    ?>
                <tr>

                </tr>
              </thead>
              <tbody>
              <?php foreach ($kelas_siswa as $ks):?>
              <tr>                    
                  <td colspan="2"><?=$ks->siswa_nama?></td>
                  <?php
                    // $jumlah_hari = getJumlahHari((int)$_POST['bulan'], (int)$_POST['tahun']);
                    $jml_hadir = 0;             
                    $jml_izin = 0;             
                    $jml_sakit = 0;             
                    $jml_alpa = 0;                               
                    for ($i=0; $i < count($bulan_list); $i++) {  
                        $bulan = $bulan_list[$i][0];
                        for ($j=1; $j<=5; $j++) {  
                            $kehadiran = getStatusHadir($ks->siswa_nis, $absen, $absen[0]->tahunajaran_kode, $bulan, $j);
                            if ($kehadiran == "H") { $jml_hadir++;} 
                            if ($kehadiran == "I") { $jml_izin++;}
                            if ($kehadiran == "S") { $jml_sakit++;}
                            if ($kehadiran == "A") { $jml_alpa++;}
                            echo "<td>".$kehadiran."</td>";                        
                        }                         
                    }
                  ?>
                  <td><?=$jml_hadir;?></td>
                  <td><?=$jml_izin;?></td>
                  <td><?=$jml_sakit;?></td>
                  <td><?=$jml_alpa;?></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>

            <!-- Bagian footer, untuk tandatangan -->
            <hr style="height: 2px; background-color: black; border: 0; color: black;">
            <table cellpadding="4" cellspacing="0" width="100%">
              <tr>
                <th style="width: 35%" style="text-align: left;">Kepala MTs DARUL HIKAN<br><br><br><br></th>
                <td style="width: 30%;">&nbsp</td>
                <th style="width: 35%" style="text-align: left;">WALI KELAS<br><br><br><br></th>
              </tr>
              <!-- <br>
              <br>
              <br>
              <br> -->
              <tr>
                <th style="width: 20%;">______________________</th>
                <td style="width: 60%"></td>
                <th style="width: 20%;">______________________</th>
              </tr>    
            </table>
          </div>
        
<?php }?>
</div>
<!--end-main-container-part-->
</body>

</html>
