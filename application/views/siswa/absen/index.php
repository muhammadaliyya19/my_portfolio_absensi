<section class="content">
    <div class="container-fluid">
      <div class="row">
        <section class="col-lg-12 connectedSortable">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><?=$title?></h3>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="myTable" class="table table-bordered table-flush text-center">
                  <thead class="thead-light">
                    <th width="1%">No</th>
                    <th>Mata Pelajaran</th>
                    <th>Guru</th>
                    <th>Kehadiran</th>
                    <th>Waktu Absen</th>
                  </thead>
                  <tbody>
                    <?php $i=1; 
                    foreach ($data as $d) {  ?>
                    <tr>
                      <td><?=$i++?></td>
                      <td><?=ucfirst($d->mapel)?></td>
                      <td><?=ucfirst($d->guru)?></td>
                      <td><?php
                          if ($d->kehadiran=="H"){
                              echo '<span class="badge badge-success">Hadir</span>';
                          }else if ($d->kehadiran=="I"){
                              echo '<span class="badge badge-warning">Izin</span>';
                          }else if ($d->kehadiran=="S"){
                              echo '<span class="badge badge-info">Sakit</span>';
                          }else if ($d->kehadiran=="A"){
                              echo '<span class="badge badge-danger">Alpa</span>';
                          }
                        ?>
                      </td>
                      <td><?=date('d F Y, H:i',strtotime($d->time))?></td>
                    </tr>
                    <?php } ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>