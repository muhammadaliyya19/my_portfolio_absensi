-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Feb 2022 pada 09.15
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gizsvmbl_absensi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `absensi_id` int(11) NOT NULL,
  `absensi_waktu` datetime NOT NULL,
  `absensi_mapelguru` int(11) NOT NULL,
  `absensi_materi` int(11) NOT NULL,
  `minggu_ke` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`absensi_id`, `absensi_waktu`, `absensi_mapelguru`, `absensi_materi`, `minggu_ke`) VALUES
(50, '2021-12-31 17:20:46', 19, 12, 1),
(51, '2022-01-25 08:47:16', 19, 12, 2),
(52, '2022-01-26 08:17:55', 19, 12, 3),
(53, '2022-01-26 08:47:55', 19, 12, 4),
(64, '2022-02-11 07:39:51', 19, 12, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE `config` (
  `config_id` int(11) NOT NULL,
  `config_logo` varchar(100) NOT NULL,
  `config_sekolah` varchar(254) NOT NULL,
  `config_alamat` text NOT NULL,
  `config_kota` varchar(100) NOT NULL,
  `config_phone` varchar(14) NOT NULL,
  `config_email` varchar(100) NOT NULL,
  `config_kepsek` varchar(254) NOT NULL,
  `config_nipkepsek` varchar(50) NOT NULL,
  `config_author` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`config_id`, `config_logo`, `config_sekolah`, `config_alamat`, `config_kota`, `config_phone`, `config_email`, `config_kepsek`, `config_nipkepsek`, `config_author`) VALUES
(1, 'dh9.jpg', 'MTs Darul Hikam', 'Jl. sukorame. 1, Srigonco, Bantur, Malang', 'Malang', '085655281615', 'mtsdhbtr41@gmail.com', 'ISTIQOMAH S.PdI', '12345678909', 'Muhammad Sirojuddin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_absensi`
--

CREATE TABLE `detail_absensi` (
  `detail_id` int(11) NOT NULL,
  `detail_absensi` int(11) NOT NULL,
  `detail_nis` varchar(50) NOT NULL,
  `detail_kehadiran` enum('H','I','S','A') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detail_absensi`
--

INSERT INTO `detail_absensi` (`detail_id`, `detail_absensi`, `detail_nis`, `detail_kehadiran`) VALUES
(93, 50, '12', 'H'),
(94, 50, '123', 'H'),
(95, 51, '1', 'H'),
(96, 51, '11', 'H'),
(97, 51, '12', 'H'),
(98, 51, '122', 'H'),
(99, 51, '123', 'H'),
(100, 51, '2', 'I'),
(101, 51, '20', 'H'),
(102, 51, '24', 'H'),
(103, 51, '3', 'I'),
(104, 51, '32', 'H'),
(105, 52, '1', 'H'),
(106, 52, '11', 'H'),
(107, 52, '12', 'I'),
(108, 52, '122', 'I'),
(109, 52, '123', 'S'),
(110, 52, '2', 'H'),
(111, 52, '20', 'H'),
(112, 52, '24', 'H'),
(113, 52, '3', 'A'),
(114, 52, '32', 'A'),
(115, 53, '1', 'H'),
(116, 53, '11', 'H'),
(117, 53, '12', 'I'),
(118, 53, '122', 'I'),
(119, 53, '123', 'S'),
(120, 53, '2', 'H'),
(121, 53, '20', 'H'),
(122, 53, '24', 'H'),
(123, 53, '3', 'A'),
(124, 53, '32', 'A'),
(185, 64, '1', 'H'),
(186, 64, '11', 'H'),
(187, 64, '12', 'H'),
(188, 64, '122', 'H'),
(189, 64, '123', 'H'),
(190, 64, '2', 'H'),
(191, 64, '20', 'H'),
(192, 64, '24', 'H'),
(193, 64, '3', 'H'),
(194, 64, '32', 'H');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `guru_nip` varchar(50) NOT NULL,
  `guru_nama` varchar(254) NOT NULL,
  `guru_jk` enum('L','P') NOT NULL,
  `guru_tanggallahir` date NOT NULL,
  `guru_tempatlahir` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`guru_nip`, `guru_nama`, `guru_jk`, `guru_tanggallahir`, `guru_tempatlahir`) VALUES
('1', 'judin', 'L', '2022-01-01', 'malang'),
('165150', 'Aliy', 'L', '1998-07-20', 'Kediri'),
('90', 'Tri', 'P', '2022-01-23', 'Mlg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `kelas_kode` varchar(10) NOT NULL,
  `kelas_tingkat` varchar(254) NOT NULL,
  `kelas_nmkelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`kelas_kode`, `kelas_tingkat`, `kelas_nmkelas`) VALUES
('KLS-01', 'VI', 'JUR01'),
('KLS-02', 'VII', 'JUR02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `komentar_id` int(11) NOT NULL,
  `komentar_relasi` int(11) NOT NULL COMMENT 'diisi id tugas/id materi',
  `komentar_user` varchar(50) DEFAULT NULL COMMENT 'diisi nis siswa,jika kosong maka komentar dari guru',
  `komentar_isi` text NOT NULL,
  `komentar_waktu` datetime NOT NULL,
  `komentar_jenis` enum('materi','tugas') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`komentar_id`, `komentar_relasi`, `komentar_user`, `komentar_isi`, `komentar_waktu`, `komentar_jenis`) VALUES
(1, 5, NULL, 'test123', '2021-04-06 14:54:18', 'materi'),
(2, 5, '12346', 'testefbk;jwe', '2021-04-06 14:56:56', 'materi'),
(3, 5, '12346', 'test', '2021-04-06 14:57:16', 'materi'),
(4, 5, '12346', 'ini percobaan', '2021-04-06 15:11:50', 'materi'),
(5, 1, NULL, 'test tugas', '2021-04-06 15:36:25', 'tugas'),
(6, 1, '12346', 'test tugas 2', '2021-04-06 15:36:33', 'tugas'),
(7, 1, '12346', 'pak tugas nya dikumpul dalam format apa? terima kasih', '2021-04-06 16:09:45', 'tugas'),
(8, 1, '12345', 'oiya saya lupa pak, maaf', '2021-04-06 16:11:26', 'tugas'),
(9, 1, NULL, 'test guru', '2021-04-06 16:26:30', 'tugas'),
(10, 5, NULL, 'test materi guru', '2021-04-06 16:30:35', 'materi'),
(11, 6, NULL, 'test nateri baru', '2021-04-06 16:31:43', 'materi'),
(12, 6, NULL, 'oke lah', '2021-04-06 16:32:12', 'materi'),
(13, 5, '12346', 'nf.', '2021-10-27 23:52:32', 'materi'),
(14, 5, '12346', 'bfbfb', '2021-10-27 23:52:41', 'materi'),
(15, 1, '12346', 'tes', '2021-11-20 00:11:00', 'tugas'),
(16, 1, '12346', 'test', '2021-11-20 00:11:19', 'tugas'),
(17, 1, NULL, 'ok', '2021-11-20 17:10:21', 'tugas'),
(18, 1, NULL, 'tes', '2021-11-20 17:11:11', 'tugas'),
(19, 10, '12346', 'saya sakit tidak bisa hadir ', '2021-12-09 02:27:14', 'materi'),
(20, 10, NULL, 'baik, semoga cepat sembuh ', '2021-12-09 02:27:54', 'materi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kumpul`
--

CREATE TABLE `kumpul` (
  `kumpul_id` int(11) NOT NULL,
  `kumpul_tugas` int(11) NOT NULL,
  `kumpul_siswa` varchar(50) NOT NULL,
  `kumpul_file` varchar(254) NOT NULL,
  `kumpul_waktu` datetime NOT NULL,
  `kumpul_nilai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `mapel_kode` varchar(50) NOT NULL,
  `mapel_nama` varchar(254) NOT NULL,
  `mapel_nmkls` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`mapel_kode`, `mapel_nama`, `mapel_nmkls`) VALUES
('MPL-00001', 'ski', 'JUR02'),
('MPL-00002', 'Prakarya', 'JUR01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `materi`
--

CREATE TABLE `materi` (
  `materi_id` int(11) NOT NULL,
  `materi_mapelguru` int(11) NOT NULL,
  `materi_judul` varchar(254) NOT NULL,
  `materi_isi` longtext NOT NULL,
  `materi_file` varchar(254) DEFAULT NULL,
  `materi_video` varchar(254) NOT NULL,
  `materi_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `materi`
--

INSERT INTO `materi` (`materi_id`, `materi_mapelguru`, `materi_judul`, `materi_isi`, `materi_file`, `materi_video`, `materi_waktu`) VALUES
(12, 19, 'sejarah kebudayaan islam', '', NULL, '', '2022-01-21 01:01:48'),
(13, 20, 'Daur ulang limbah', '', NULL, '', '2022-01-25 20:50:48'),
(14, 20, 'Nilai tambah pada benda sekitar', '', NULL, '', '2022-01-25 20:51:28'),
(15, 21, 'Materi Pertama', '', NULL, '', '2022-01-25 21:14:24'),
(16, 21, 'Materi Kedua', '', NULL, '', '2022-01-25 21:14:39'),
(17, 22, 'Materi Pertama', '', NULL, '', '2022-01-26 07:31:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nama_kelas`
--

CREATE TABLE `nama_kelas` (
  `nmkls_kode` varchar(10) NOT NULL,
  `nmkls_nama` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nama_kelas`
--

INSERT INTO `nama_kelas` (`nmkls_kode`, `nmkls_nama`) VALUES
('10', 'J'),
('11', 'K'),
('12', 'L'),
('13', 'M'),
('14', 'N'),
('15', 'O'),
('16', 'P'),
('3', 'C'),
('4', 'D'),
('5', 'E'),
('6', 'F'),
('7', 'G'),
('8', 'H'),
('9', 'I'),
('JUR01', 'A'),
('JUR02', 'B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman_id` int(11) NOT NULL,
  `pengumuman_judul` varchar(254) NOT NULL,
  `pengumuman_isi` longtext NOT NULL,
  `pengumuman_waktu` datetime NOT NULL,
  `pengumuman_img` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan`
--

CREATE TABLE `pesan` (
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `guru` varchar(250) NOT NULL,
  `mapel` varchar(250) NOT NULL,
  `kehadiran` enum('H','I','S','A') NOT NULL,
  `status` enum('MENUNGGU JADWAL','GAGAL','TERKIRIM') NOT NULL DEFAULT 'MENUNGGU JADWAL',
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pesan`
--

INSERT INTO `pesan` (`id`, `nama`, `nomor`, `guru`, `mapel`, `kehadiran`, `status`, `time`) VALUES
(63, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-21 01:02:40'),
(64, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-24 13:03:28'),
(65, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-24 17:20:46'),
(66, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-24 17:20:46'),
(67, 'Edo', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(68, 'Edotensei', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(69, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(70, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(71, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(72, 'Onky', '085655281615', 'Judin', 'ski', 'I', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(73, 'Oni', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(74, 'Nia', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(75, 'Fera', '085655281615', 'Judin', 'ski', 'I', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(76, 'Tatiana', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-25 08:47:16'),
(77, 'Edo', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(78, 'Edotensei', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(79, 'Ilmi', '085655281615', 'Judin', 'ski', 'I', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(80, 'Ilmi', '085655281615', 'Judin', 'ski', 'I', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(81, 'Judin', '085655281615', 'Judin', 'ski', 'S', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(82, 'Onky', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(83, 'Oni', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(84, 'Nia', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(85, 'Fera', '085655281615', 'Judin', 'ski', 'A', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(86, 'Tatiana', '085655281615', 'Judin', 'ski', 'A', 'MENUNGGU JADWAL', '2022-01-26 20:56:55'),
(87, 'Edo', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(88, 'Edotensei', '085655281615', 'Judin', 'ski', 'I', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(89, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(90, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(91, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(92, 'Onky', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(93, 'Oni', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(94, 'Nia', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(95, 'Fera', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(96, 'Tatiana', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:09:44'),
(97, 'Edo', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(98, 'Edotensei', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(99, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(100, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(101, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(102, 'Onky', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(103, 'Oni', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(104, 'Nia', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(105, 'Fera', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(106, 'Tatiana', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:34:20'),
(107, 'Edo', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(108, 'Edotensei', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(109, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(110, 'Ilmi', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(111, 'Judin', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(112, 'Onky', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(113, 'Oni', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(114, 'Nia', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(115, 'Fera', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51'),
(116, 'Tatiana', '085655281615', 'Judin', 'ski', 'H', 'MENUNGGU JADWAL', '2022-02-11 07:39:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `relasi_kelassiswa`
--

CREATE TABLE `relasi_kelassiswa` (
  `kelassiswa_id` int(11) NOT NULL,
  `kelassiswa_siswa` varchar(50) NOT NULL,
  `kelassiswa_kelas` varchar(10) NOT NULL,
  `kelassiswa_tahunajaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `relasi_kelassiswa`
--

INSERT INTO `relasi_kelassiswa` (`kelassiswa_id`, `kelassiswa_siswa`, `kelassiswa_kelas`, `kelassiswa_tahunajaran`) VALUES
(9, '123', 'KLS-01', 'TA0001'),
(10, '12', 'KLS-01', 'TA0001'),
(11, '1', 'KLS-01', 'TA0001'),
(12, '11', 'KLS-01', 'TA0001'),
(13, '122', 'KLS-01', 'TA0001'),
(14, '2', 'KLS-01', 'TA0001'),
(15, '20', 'KLS-01', 'TA0001'),
(16, '24', 'KLS-01', 'TA0001'),
(17, '3', 'KLS-01', 'TA0001'),
(18, '32', 'KLS-01', 'TA0001'),
(19, '456', 'KLS-02', 'TA0001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `relasi_mapelguru`
--

CREATE TABLE `relasi_mapelguru` (
  `mapelguru_id` int(11) NOT NULL,
  `mapelguru_guru` varchar(10) NOT NULL,
  `mapelguru_mapel` varchar(10) NOT NULL,
  `mapelguru_kelas` varchar(10) NOT NULL,
  `mapelguru_tahunajaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `relasi_mapelguru`
--

INSERT INTO `relasi_mapelguru` (`mapelguru_id`, `mapelguru_guru`, `mapelguru_mapel`, `mapelguru_kelas`, `mapelguru_tahunajaran`) VALUES
(19, '1', 'MPL-00001', 'KLS-01', 'TA0001'),
(20, '165150', 'MPL-00002', 'KLS-01', 'TA0001'),
(21, '90', 'MPL-00002', 'KLS-02', 'TA0001'),
(22, '1', 'MPL-00001', 'KLS-02', 'TA0001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `siswa_nis` varchar(50) NOT NULL,
  `siswa_nama` varchar(254) NOT NULL,
  `siswa_jk` enum('L','P') NOT NULL,
  `siswa_tanggallahir` date NOT NULL,
  `siswa_tempatlahir` varchar(254) NOT NULL,
  `siswa_wa` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`siswa_nis`, `siswa_nama`, `siswa_jk`, `siswa_tanggallahir`, `siswa_tempatlahir`, `siswa_wa`) VALUES
('1', 'Edo', 'L', '2022-01-01', 'malang', '085655281615'),
('11', 'Edotensei', 'L', '2022-01-01', 'malang', '085655281615'),
('12', 'Ilmi', 'L', '2022-01-01', 'malang', '085655281615'),
('122', 'Ilmi', 'L', '2022-01-01', 'malang', '085655281615'),
('123', 'Judin', 'L', '2022-01-01', 'malang', '085655281615'),
('2', 'Onky', 'L', '2022-01-01', 'malang', '085655281615'),
('20', 'Oni', 'L', '2022-01-01', 'malang', '085655281615'),
('24', 'Nia', 'P', '2022-01-01', 'malang', '085655281615'),
('3', 'Fera', 'L', '2022-01-01', 'malang', '085655281615'),
('32', 'Tatiana', 'P', '2022-01-01', 'malang', '085655281615'),
('456', 'Dani', 'L', '2022-01-23', 'Kediri', '085784114455');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahunajaran`
--

CREATE TABLE `tahunajaran` (
  `tahunajaran_kode` varchar(10) NOT NULL,
  `tahunajaran_nama` varchar(254) NOT NULL,
  `tahunajaran_status` enum('T','F') NOT NULL COMMENT 'T = true, F = False'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tahunajaran`
--

INSERT INTO `tahunajaran` (`tahunajaran_kode`, `tahunajaran_nama`, `tahunajaran_status`) VALUES
('TA0001', 'Genap 2020 / 2021', 'T'),
('TA0002', 'Ganjil 2020 / 2021', 'F'),
('TA0004', 'Ganjil 2022 / 2023', 'F');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tugas`
--

CREATE TABLE `tugas` (
  `tugas_id` int(11) NOT NULL,
  `tugas_mapelguru` int(11) NOT NULL,
  `tugas_judul` varchar(254) NOT NULL,
  `tugas_isi` longtext NOT NULL,
  `tugas_batas` datetime NOT NULL,
  `tugas_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tugas`
--

INSERT INTO `tugas` (`tugas_id`, `tugas_mapelguru`, `tugas_judul`, `tugas_isi`, `tugas_batas`, `tugas_waktu`) VALUES
(4, 20, 'Tugas pertama', '', '2022-01-27 20:52:00', '2022-01-25 20:52:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(254) NOT NULL,
  `user_password` varchar(254) NOT NULL,
  `user_role` enum('admin','guru','siswa') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_password`, `user_role`) VALUES
(1, 'administrator', '202cb962ac59075b964b07152d234b70', 'admin'),
(9, '1', 'c4ca4238a0b923820dcc509a6f75849b', 'guru'),
(10, '123', '202cb962ac59075b964b07152d234b70', 'siswa'),
(11, '12', 'c20ad4d76fe97759aa27a0c99bff6710', 'siswa'),
(12, '32', '6364d3f0f495b6ab9dcf8d3b5c6e0b01', 'siswa'),
(13, '11', '6512bd43d9caa6e02c990b0a82652dca', 'siswa'),
(14, '3', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 'siswa'),
(15, '122', 'a0a080f42e6f13b3a2df133f073095dd', 'siswa'),
(16, '24', '1ff1de774005f8da13f42943881c655f', 'siswa'),
(17, '20', '98f13708210194c475687be6106a3b84', 'siswa'),
(18, '2', 'c81e728d9d4c2f636f067f89cc14862c', 'siswa'),
(19, '165150', '42ffcd98701d75a183c4e52e303c8a35', 'guru'),
(20, '456', '250cf8b51c773f3f8dc8b4be867a9a02', 'siswa'),
(21, '90', '8613985ec49eb8f757ae6439e879bb2a', 'guru');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`absensi_id`),
  ADD KEY `absensi_materi` (`absensi_materi`),
  ADD KEY `absensi_mapelguru` (`absensi_mapelguru`);

--
-- Indeks untuk tabel `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`config_id`);

--
-- Indeks untuk tabel `detail_absensi`
--
ALTER TABLE `detail_absensi`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `detail_absensi` (`detail_absensi`),
  ADD KEY `detail_nis` (`detail_nis`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`guru_nip`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kelas_kode`),
  ADD KEY `kelas_jurusan` (`kelas_nmkelas`);

--
-- Indeks untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`komentar_id`);

--
-- Indeks untuk tabel `kumpul`
--
ALTER TABLE `kumpul`
  ADD PRIMARY KEY (`kumpul_id`),
  ADD KEY `kumpul_siswa` (`kumpul_siswa`),
  ADD KEY `kumpul_tugas` (`kumpul_tugas`);

--
-- Indeks untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`mapel_kode`),
  ADD KEY `mapel_jurusan` (`mapel_nmkls`);

--
-- Indeks untuk tabel `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`materi_id`),
  ADD KEY `materi_mapelguru` (`materi_mapelguru`);

--
-- Indeks untuk tabel `nama_kelas`
--
ALTER TABLE `nama_kelas`
  ADD PRIMARY KEY (`nmkls_kode`);

--
-- Indeks untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`pengumuman_id`);

--
-- Indeks untuk tabel `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `relasi_kelassiswa`
--
ALTER TABLE `relasi_kelassiswa`
  ADD PRIMARY KEY (`kelassiswa_id`),
  ADD KEY `kelassiswa_siswa` (`kelassiswa_siswa`),
  ADD KEY `kelassiswa_kelas` (`kelassiswa_kelas`),
  ADD KEY `kelassiswa_tahunajaran` (`kelassiswa_tahunajaran`);

--
-- Indeks untuk tabel `relasi_mapelguru`
--
ALTER TABLE `relasi_mapelguru`
  ADD PRIMARY KEY (`mapelguru_id`),
  ADD KEY `mapelguru_guru` (`mapelguru_guru`),
  ADD KEY `mapelguru_mapel` (`mapelguru_mapel`),
  ADD KEY `mapelguru_tahunajaran` (`mapelguru_tahunajaran`),
  ADD KEY `mapelguru_kelas` (`mapelguru_kelas`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`siswa_nis`);

--
-- Indeks untuk tabel `tahunajaran`
--
ALTER TABLE `tahunajaran`
  ADD PRIMARY KEY (`tahunajaran_kode`);

--
-- Indeks untuk tabel `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`tugas_id`),
  ADD KEY `tugas_mapelguru` (`tugas_mapelguru`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absensi`
--
ALTER TABLE `absensi`
  MODIFY `absensi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `config`
--
ALTER TABLE `config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `detail_absensi`
--
ALTER TABLE `detail_absensi`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT untuk tabel `komentar`
--
ALTER TABLE `komentar`
  MODIFY `komentar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kumpul`
--
ALTER TABLE `kumpul`
  MODIFY `kumpul_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `materi`
--
ALTER TABLE `materi`
  MODIFY `materi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT untuk tabel `relasi_kelassiswa`
--
ALTER TABLE `relasi_kelassiswa`
  MODIFY `kelassiswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `relasi_mapelguru`
--
ALTER TABLE `relasi_mapelguru`
  MODIFY `mapelguru_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `tugas`
--
ALTER TABLE `tugas`
  MODIFY `tugas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`absensi_mapelguru`) REFERENCES `relasi_mapelguru` (`mapelguru_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `absensi_ibfk_2` FOREIGN KEY (`absensi_materi`) REFERENCES `materi` (`materi_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_absensi`
--
ALTER TABLE `detail_absensi`
  ADD CONSTRAINT `detail_absensi_ibfk_1` FOREIGN KEY (`detail_nis`) REFERENCES `siswa` (`siswa_nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`kelas_nmkelas`) REFERENCES `nama_kelas` (`nmkls_kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kumpul`
--
ALTER TABLE `kumpul`
  ADD CONSTRAINT `kumpul_ibfk_1` FOREIGN KEY (`kumpul_siswa`) REFERENCES `siswa` (`siswa_nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kumpul_ibfk_2` FOREIGN KEY (`kumpul_tugas`) REFERENCES `tugas` (`tugas_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD CONSTRAINT `mapel_ibfk_1` FOREIGN KEY (`mapel_nmkls`) REFERENCES `nama_kelas` (`nmkls_kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `materi_ibfk_1` FOREIGN KEY (`materi_mapelguru`) REFERENCES `relasi_mapelguru` (`mapelguru_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `relasi_kelassiswa`
--
ALTER TABLE `relasi_kelassiswa`
  ADD CONSTRAINT `relasi_kelassiswa_ibfk_1` FOREIGN KEY (`kelassiswa_kelas`) REFERENCES `kelas` (`kelas_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relasi_kelassiswa_ibfk_2` FOREIGN KEY (`kelassiswa_siswa`) REFERENCES `siswa` (`siswa_nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relasi_kelassiswa_ibfk_3` FOREIGN KEY (`kelassiswa_tahunajaran`) REFERENCES `tahunajaran` (`tahunajaran_kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `relasi_mapelguru`
--
ALTER TABLE `relasi_mapelguru`
  ADD CONSTRAINT `relasi_mapelguru_ibfk_1` FOREIGN KEY (`mapelguru_guru`) REFERENCES `guru` (`guru_nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relasi_mapelguru_ibfk_2` FOREIGN KEY (`mapelguru_mapel`) REFERENCES `mapel` (`mapel_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relasi_mapelguru_ibfk_3` FOREIGN KEY (`mapelguru_tahunajaran`) REFERENCES `tahunajaran` (`tahunajaran_kode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relasi_mapelguru_ibfk_4` FOREIGN KEY (`mapelguru_kelas`) REFERENCES `kelas` (`kelas_kode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tugas`
--
ALTER TABLE `tugas`
  ADD CONSTRAINT `tugas_ibfk_1` FOREIGN KEY (`tugas_mapelguru`) REFERENCES `relasi_mapelguru` (`mapelguru_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
